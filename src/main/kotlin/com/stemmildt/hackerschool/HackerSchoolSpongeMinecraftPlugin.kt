package com.stemmildt.hackerschool

import org.spongepowered.api.Sponge
import org.spongepowered.api.block.BlockTypes
import org.spongepowered.api.command.CommandException
import org.spongepowered.api.command.CommandResult
import org.spongepowered.api.command.CommandSource
import org.spongepowered.api.command.args.CommandContext
import org.spongepowered.api.command.spec.CommandExecutor
import org.spongepowered.api.command.spec.CommandSpec
import org.spongepowered.api.data.key.Keys
import org.spongepowered.api.data.type.HandTypes
import org.spongepowered.api.entity.EntityTypes
import org.spongepowered.api.entity.living.player.Player
import org.spongepowered.api.event.Listener
import org.spongepowered.api.event.block.InteractBlockEvent
import org.spongepowered.api.event.entity.MoveEntityEvent
import org.spongepowered.api.event.game.state.GameStartedServerEvent
import org.spongepowered.api.item.ItemTypes
import org.spongepowered.api.item.inventory.ItemStack
import org.spongepowered.api.plugin.Plugin
import org.spongepowered.api.text.Text
import org.spongepowered.api.world.Location
import org.spongepowered.api.world.World
import java.util.concurrent.TimeUnit

@Plugin(
  id = "hacker-school-sponge-minecraft-plugin",
  name = "Hacker School Sponge Minecraft Plugin",
  version = "1.0.0-SNAPSHOT"
)
class HackerSchoolSpongeMinecraftPlugin {

  @Listener
  fun onGameStartedServer(gameStartedServerEvent: GameStartedServerEvent) {
    val helloWorldCommandSpec = CommandSpec.builder().executor(HelloWorldCommand()).build()
    Sponge.getCommandManager().register(this, helloWorldCommandSpec, "helloworld", "hello", "test");

    val spawnChickenCommandSpec = CommandSpec.builder().executor(SpawnChickenCommand()).build()
    Sponge.getCommandManager().register(this, spawnChickenCommandSpec, "chicken")

    val giveSwordCommandSpec = CommandSpec.builder().executor(GiveSwordCommand()).build()
    Sponge.getCommandManager().register(this, giveSwordCommandSpec, "sword")

    val spawnAreaCommandSpec = CommandSpec.builder().executor(SpawnAreaCommand()).build()
    Sponge.getCommandManager().register(this, spawnAreaCommandSpec, "area")
  }

  @Listener
  fun onInteractBlock(interactBlockEvent: InteractBlockEvent.Primary) {
    if (interactBlockEvent.cause.any { it is Player }) {
      val player = interactBlockEvent.cause.last(Player::class.java).get()
      player.sendMessage(Text.of("You clicked a block ${interactBlockEvent.targetBlock.state.type.name}"))
    }
  }

  @Listener
  fun onMoveEntity(moveEntityEvent: MoveEntityEvent) {
    val entity = moveEntityEvent.targetEntity
    val blockBelowEntity = entity.location.add(0.0, -1.0, 0.0)
    if (entity.type == EntityTypes.PLAYER) {
      if (blockBelowEntity.blockType == BlockTypes.STONE) {
        Sponge.getScheduler().createTaskBuilder()
          .execute(Runnable { blockBelowEntity.blockType = BlockTypes.REDSTONE_BLOCK })
          .delay(500, TimeUnit.MILLISECONDS)
          .submit(this)
      }
      if (blockBelowEntity.blockType == BlockTypes.REDSTONE_BLOCK) {
        Sponge.getScheduler().createTaskBuilder()
          .execute(Runnable { blockBelowEntity.blockType = BlockTypes.AIR })
          .delay(500, TimeUnit.MILLISECONDS)
          .submit(this)
      }
    }
  }
}

class HelloWorldCommand : CommandExecutor {

  @Throws(CommandException::class)
  override fun execute(commandSource: CommandSource, commandContext: CommandContext): CommandResult {
    commandSource.sendMessage(Text.of("Hello and welcome to my server!"))
    return CommandResult.success()
  }
}

class SpawnChickenCommand : CommandExecutor {

  override fun execute(src: CommandSource, args: CommandContext): CommandResult {
    src.sendMessage(Text.of("hallo!"))
    if (src is Player) {
      val location = src.location
      val world = location.extent
      repeat(30) {
        val chicken = world.createEntity(EntityTypes.CHICKEN, location.position)
        chicken.offer(Keys.IS_ADULT, false)
        world.spawnEntity(chicken)
      }
    }
    return CommandResult.success()
  }
}

class GiveSwordCommand : CommandExecutor {

  override fun execute(src: CommandSource, args: CommandContext): CommandResult {
    if (src is Player) {
      val superMegaAwesomeSword = ItemStack.builder().itemType(ItemTypes.DIAMOND_SWORD).build()
      src.setItemInHand(HandTypes.MAIN_HAND, superMegaAwesomeSword)
    }
    return CommandResult.success()
  }
}

class SpawnAreaCommand : CommandExecutor {

  override fun execute(src: CommandSource, args: CommandContext): CommandResult {
    src.sendMessage(Text.of("area!"))
    if (src is Player) {
      setToSponge(src.location)
    }
    return CommandResult.success()
  }
}

fun setToSponge(location: Location<World>) {
  repeat(30) { x ->
    repeat(30) { z ->
      location.add(x.toDouble(), 0.0, z.toDouble()).blockType = BlockTypes.STONE
    }
  }
}
