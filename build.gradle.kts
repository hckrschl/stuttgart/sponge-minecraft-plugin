import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

version = "1.0.0-SNAPSHOT"

buildscript {
  project.apply {
    from("$rootDir/gradle/dependencies.gradle.kts")
  }
  val gradlePlugins = extra["gradlePlugins"] as Map<*, *>

  repositories {
    maven { setUrl("https://plugins.gradle.org/m2/") }
    maven { setUrl("https://repo.spongepowered.org/maven") }
    mavenCentral()
    jcenter()
    maven { setUrl("https://oss.sonatype.org/content/repositories/snapshots/") }
    maven { setUrl("https://jitpack.io") }
  }
  dependencies {
    classpath(gradlePlugins["kotlin-gradle-plugin"] as String)
    classpath(gradlePlugins["versions"] as String)
  }
}

apply {
  plugin("idea")
  plugin("kotlin")
  plugin("com.github.ben-manes.versions")
}

repositories {
  maven { setUrl("https://repo.spongepowered.org/maven") }
  mavenCentral()
  jcenter()
  maven { setUrl("https://oss.sonatype.org/content/repositories/snapshots/") }
  maven { setUrl("https://jitpack.io") }
}

val versions = extra["versions"] as Map<*, *>
val libraries = extra["libraries"] as Map<*, *>
val testLibraries = extra["testLibraries"] as Map<*, *>

ext["groovy.version"] = versions["groovy"]

val compile by configurations

dependencies {
  compile(libraries["spongeapi"] as String)

  compile(libraries["kotlin-stdlib-jre8"] as String)
  compile(libraries["kotlin-reflect"] as String)
}

apply {
  from("$rootDir/gradle/test.gradle.kts")
}

configure<JavaPluginConvention> {
  sourceCompatibility = JavaVersion.VERSION_1_8
  targetCompatibility = JavaVersion.VERSION_1_8
}

tasks {
  withType<KotlinCompile> {
    kotlinOptions {
      freeCompilerArgs = listOf("-Xjsr305=strict")
      jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
  }
}
