Hackerschool Sponge Minecraft Plugin
------------------------------------

Setting Up Sponge
-----------------
1. Download [Sponge Vanilla](https://www.spongepowered.org/downloads/spongevanilla/stable/)
2. Put it in it's own directory (because it will create a bunch of files and directories in there)
3. Run it once via `java -jar spongevanilla-*.jar` and stop it again (e.g. with `ctrl-c`)
4. Set `eula=true` in `eula.txt`

Setting Up IntelliJ IDEA (2018.3)
---------------------------------
1. Create a new Project from existing sources:
    * ![](images/01NewProject.png)
2. Choose Gradle as external model
    * ![](images/02Gradle.png)
3. Go to File > Prject Structure
    * ![](images/03ProjectStructure.png)
4. Create a JAR artifact from module with dependencies
    * ![](images/04Artifact.png)
5. Choose the main module 
    * ![](images/05MainModule.png)
6. Choose the `mods` subdirectory of Sponge Server directory as output and check `Include in project build` (this will automatically create the artifact when building the project)
    * ![](images/06OutputDirectory.png)
7. Click Add Configuration
    * ![](images/07AddConfiguration.png)
8. Add a new Run Configuration of type "JAR Application"
    * ![](images/08JAR-Application.png)
9. Tell it to run the Sponge Vanilla JAR and set the working directory to the directory where the server is in.
    * ![](images/09RunConfiguration.png)
10. Add the build artifact to "Before Launch"
    * ![](images/10BuildArtifact.png)
    * ![](images/11MainArtifact.png)
11. Now you can build the plugin and run the server
    * ![](images/12RunSponge.png)
